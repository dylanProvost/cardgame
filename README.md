# AOE Card game

Welcome to my version of the AOE card game.
To install all the dependencies run
>npm install

In the app folder.

The project is divided in mainly 3 different folders. the `app` folder contains the JavaScript files `index.js` runs the app. the `json` folder contains the mocked data of the players and the `scss` folder the styles.

The project can be run and compiled through webpack, please run the dev environment with the command
>npm run start

And enjoy the app.

Hopefully I have met all the requirements, and have set up the environment so that it would be possible to minify, create production releases and tests. I have however not implemented those myself, due to a lack of time, mostly. But some packages are already installed such as `jasmine`, `karma` and `uglifyjs` for webpack.

## Choices
I made a few choices which I'd like to explain.

The first one is the use of template literals. I could have used templating tools (and it would have probably been a bit cleaner in some modules) but I preferred doing it that way because I didn't see a huge difference (not too many lines of HTML) and also because template literals are **A LOT** faster than templates.

I've also chosen to use event emitters to spread the data across modules. Surely there are many ways to solve that problem, but I wanted to play around with those, and that was the main reason!

I also focused a bit more on the JavaScript than on the styles. But I still changed a little bit the mocked design given to make it a bit more attractive. It is really just Bootstrap and a few lines of Scss.


**I'd love some feedbacks on the project so please let me know what you think and any ideas to improve the work you could think of it :)**
