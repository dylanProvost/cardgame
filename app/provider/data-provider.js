"use strict";

import $ from "jquery";

class DataProvider{

    constructor(){

    }

    retrieveData(url){
        return new Promise ((resolve) => {
            $.ajax({
                url: url,
                type: "GET",
                success: function (result) {
                    resolve(result);
                },
                error: function(err){
                    console.log(err)
                }
            });
        });
    }

    submitData(url, data){
        return $.ajax({
            url: url,
            type: "POST",
            data: JSON.stringify(data),
            success: function (result) {
                return result;
            }
        });
    }

}

export default DataProvider;
