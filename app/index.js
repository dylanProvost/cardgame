"use strict";

import scss from "../scss/main.scss"

import Overview from "./src/overview";
import Controls from "./src/controls";
import Details from "./src/details";

import EventEmitter from "eventemitter3";

import Player from "./model/player";

import DataProvider from "./provider/data-provider";

let dataProvider = new DataProvider();
dataProvider.retrieveData("./json/data.json").then((result) => {
    let dataset = result.map((item) => new Player(item.name, item.player, item.asset));

    let emitter = new EventEmitter();
    // watch event for details coming back from card
    emitter.on("updateDetails", (currentPlayer) => {
        //when data received set details data and html and show it
        details.player = currentPlayer;
        details.setHtml();
        details.show();
    });
    // watch event for changes in dataset coming from controls
    emitter.on("updateDataSet", (array) => {
        overview.updateCards();
        overview.displayHtml();
    });

    let overview = new Overview(dataset, emitter);
    //start displaying the cards inside the overview
    overview.displayHtml();

    let controls = new Controls(dataset, emitter);
    //display the controls elements
    controls.show();
    //set events once controls have been created
    controls.setEvents();

    //initialise a details component
    let details = new Details();

}).catch(err => console.log(err))
