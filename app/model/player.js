"use strict";

class Player{
    constructor(name, player, asset){
        this._name = name;
        this._playerName = player;
        this._asset = asset;
    }

    get name(){ return this._name; }
    get playerName(){ return this._playerName; }
    get asset(){ return this._asset; }

    set name(name){ this._name = name; }
    set playerName(playerName){ this._playerName = playerName; }
    set asset(asset){ this._asset = asset; }
}

export default Player;
