"use strict";

import $ from "jquery";
import DataProvider from "../provider/data-provider";

class Controls{

    constructor(data, eventemitter){
        this._data = data;
        this._eventemitter = eventemitter;

        this._html = this.setHtml();
    }

    sortDesc(){
        this._data.sort((a, b) => {
            return b.name > a.name ? 1 : b.name > a.name ? -1 : 0;
        });
    }

    sortAsc(){
        this._data.sort((a, b) => {
            return b.name < a.name ? 1 : b.name > a.name ? -1 : 0;
        });
    }

    submit(data){
        let dataProvider = new DataProvider();
        //mocked api call
        dataProvider.submitData("https://jsonplaceholder.typicode.com/posts",data).then(res => {
            $("#controls .alert").remove();
            $("#controls").append("<div class=\"alert alert-success\" role=\"alert\"><strong>Well done!</strong> The data has been submitted.</div>");
        }).catch(err => {
            $("#controls .alert").remove();
            $("#controls").append("<div class=\"alert alert-danger\" role=\"alert\"><strong>Oups!</strong> Sorry something went wrong, try again.</div>");
        });
    }

    setHtml(){
        return "<div>"+
            "<div class=\"row\">"+
                "<div class=\"col\">"+
                    `<button class=\"btn btn-outline-primary btn-lg btn-block\" id=\"sort-asc\">Sort Asc</button>`+
                "</div>"+
                "<div class=\"col\">"+
                    `<button class=\"btn btn-outline-primary btn-lg btn-block\" id=\"sort-desc\">Sort Desc</button>`+
                "</div>"+
            "</div>"+
            `<button class=\"btn btn-outline-primary btn-lg btn-block\" type=\"submit\" id=\"submit\">Submit</button>`+
        "</div>";
    }

    show(){
        $("#controls").append(this._html);
    }

    setEvents(){
        $("#sort-asc").click(() => {
          this.sortAsc();
          this._eventemitter.emit("updateDataSet", this._data);
        });
        $("#sort-desc").click(() => {
          this.sortDesc();
          this._eventemitter.emit("updateDataSet", this._data);
        });
        $("#submit").click(() => this.submit());
    }
}

export default Controls;
