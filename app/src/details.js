"use strict";

import $ from "jquery";

class Details{

    constructor(){
    }

    get player(){
        return this._player;
    }

    set player(item){
        this._player = item;
    }

    setHtml(){
      this._html = `<li class="list-group-item"><strong>${this._player.name}</strong></li>`+
                `<li class="list-group-item">${this._player.playerName}</li>`+
                `<li class="list-group-item">${this._player.asset}</li>`;
    }

    show(){
        $("#details-container").html(this._html)
        $("#details").slideDown( "slow", () => {});
    }
}

export default Details;
