"use strict";

import Card from "./card";
import $ from "jquery";

class Overview{

    constructor(data, eventemitter){
        this._data = data;
        this._cards = [];
        //create an array of cards for the view
        this._data.forEach((item, index) => {
            let name = "card" + index;
            this._cards.push(new Card(name, item, eventemitter));
        });
    }

    get data(){
        return this._data;
    }

    set data(data){
        this._data = data;
        console.log(this._data);
    }

    updateCards(){
        //updates the cards with new data
        this._cards.forEach((card, index) => {
            card._player = this._data[index];
            card.setHtml();
        })
    }

    displayHtml(){
        //display the cards in the container
        $("#overview-container").html("");
        this._cards.forEach((card) => {
            $("#overview-container").append(card.html);
            //set click events for each cards once created
            card.setEvent();
        })
    }
}

export default Overview;
