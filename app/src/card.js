"use strict";

import $ from "jquery";

class Card{

    constructor(id, item, eventemitter){
        this._id = id;
        this._player = item;
        this._eventemitter = eventemitter;

        this.setHtml();
    }

    get player(){
        return this._player;
    }

    set player(data) {
        this._player = data;
    }

    get html(){
        return this._html;
    }

    setHtml(){
        this._html = `<div class=\"card\" id=\"${this._id}\">`+
            `<ul class=\"list-group list-group-flush\">`+
                `<li class=\"card__content list-group-item\"><strong>${this._player.name}</strong></li>`+
                `<li class=\"card__content list-group-item\">${this._player.playerName}</li>`+
                `<li class=\"card__content list-group-item\">${this._player.asset}</li>`+
            `</ul>`+
        `</div>`;
    }

    setEvent(){
        $("#" + this._id).click((e) => {
            $(".card.active").removeClass("active"); //clean any previous classes
            $(e.currentTarget).addClass("active"); // select card
            // emit event to spread data
            this._eventemitter.emit("updateDetails", this._player);
        });
    }
}

export default Card;
