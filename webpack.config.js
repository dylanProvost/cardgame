var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports={
    entry: ["./app/index.js", "./scss/main.scss"],
    output: {
        filename: "bundle.js"
    },
    watch: true,
    module:{
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: "eslint-loader",
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: "babel-loader",
                query: {
                    presets: ["es2015"]
                }
            },
            {
                test: /\.scss$/,
                loaders: ["style", "css", "sass"]
            }
        ],
        rules: [
            {
                test: /\.(sass|scss)$/,
                use: ExtractTextPlugin.extract({use: ['css-loader','sass-loader']})
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin('./dist/main.css'),
    ],
    devtool: "inline-source-map"
};
